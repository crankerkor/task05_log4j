package com.oleksandr;

import org.apache.logging.log4j.*;

public class Main {
    private static Logger logger1 = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        logger1.debug("this is debug message");
        logger1.info("this is info msg");
        logger1.warn("warning");
        logger1.error("error");
        logger1.fatal("fatal uuuuu");
    }
}
