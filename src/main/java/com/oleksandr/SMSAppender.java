package com.oleksandr;

import java.io.Serializable;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;

@Plugin(name = "SMS", category = "Core", elementType = "appender", printObject = true)
public final class SMSAppender extends AbstractAppender {

    protected SMSAppender(String name, Filter filter,
                          Layout<? extends Serializable> layout, final boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    public void append(LogEvent logEvent) {
        try {
            SMSSender.send(new String(getLayout().toByteArray(logEvent)));
        } catch (Exception ex) {
            System.out.println("exception" + ex);
        }
    }

    @PluginFactory
    public static SMSAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginElement("otherAttribute") String otherAttribute) {
        if(name == null) {
            LOGGER.error("No name provided for custom appender");
            return null;
        }
        if(layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }

        return new SMSAppender(name, filter, layout, true);
    }


}
